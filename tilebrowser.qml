RowBox {
    ColumnBox {
        width: parent.width * 0.6
        WebTile {
            reloadInterval: 15
            initialUrl: ""
        }
    }
    ColumnBox {
        width: parent.width * 0.6
        WebTile {
            height: parent.height * 0.6
            reloadInterval: 5
            initialUrl: ""
        }
        WebTile {
            height: parent.height * 0.4
            reloadInterval: 10
            initialUrl: ""
        }
    }
}
