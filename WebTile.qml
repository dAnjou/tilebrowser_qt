import QtWebEngine 1.8

WebEngineView {
    property int reloadInterval: 0
    property string initialUrl: ""
    objectName: "webtile"
}
