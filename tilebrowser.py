import sys

from PySide2.QtQuick import QQuickView
from PySide2.QtWebEngine import QtWebEngine
from PySide2.QtWidgets import (
    QApplication,
    QWidget,
    QPushButton,
    QVBoxLayout
)
from PySide2.QtCore import QUrl, QTimer, QObject


class WebTile:
    def __init__(self, webtile):
        print(webtile)
        self._webtile = webtile
        self._count = 1
        self._reload_interval = webtile.property('reloadInterval')
        self._url = webtile.property('initialUrl')
        self._webtile.setProperty('url', self._url)

    def reload(self):
        if self._reload_interval > 0:
            if self._count == self._reload_interval:
                self._webtile.setProperty('url', self._url)
                self._count = 1
            else:
                self._count += 1


class WebTileReloader(QTimer):
    def __init__(self, *args, **kwargs):
        super(WebTileReloader, self).__init__(*args, **kwargs)
        self.timeout.connect(self._reload)
        self._webtiles = []

    def add_webtile(self, webtile):
        self._webtiles.append(webtile)

    def _reload(self):
        for webtile in self._webtiles:
            webtile.reload()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    QtWebEngine.initialize()

    view = QQuickView()
    view.setSource(QUrl('tilebrowser.qml'))
    view.setResizeMode(QQuickView.SizeRootObjectToView)

    widget = QWidget()
    widget.setWindowTitle("Dashboard")
    widget.setMinimumSize(app.desktop().size() / 2)

    reloader = WebTileReloader(app)
    for webtile in view.findChildren(QObject, 'webtile'):
        reloader.add_webtile(WebTile(webtile))

    def start_reloading():
        widget.showFullScreen()
        reloader.start(1000)
        button.deleteLater()

    button = QPushButton("Start reloading")
    button.clicked.connect(start_reloading)

    layout = QVBoxLayout()
    layout.setMargin(0)
    layout.addWidget(button)
    layout.addWidget(QWidget.createWindowContainer(view))

    widget.setLayout(layout)
    widget.showMaximized()

    sys.exit(app.exec_())
